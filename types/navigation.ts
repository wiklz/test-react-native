export type RootStackParamList = {
  Home: undefined;
  Auth: {type: 'registration' | 'login'};
};
