import {Appearance} from 'react-native';
const theme = Appearance.getColorScheme();
const Colors = {
  primary: theme === 'dark' ? '#fafafa' : '#262626',
  secondary: '#13c2c2',
  invertPrimary: theme === 'dark' ? '#262626' : '#fafafa',
  buttonText: '#fafafa',
};

export default Colors;
