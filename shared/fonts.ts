const Fonts = {
  regular: 'AvenirNextLight',
  light: 'AvenirNextThin',
  title: 'AvenirNextDemi',
  logo: 'RenogareSoft-Regular',
  sizePrimary: 14,
  sizeTitle: 20,
};

export default Fonts;
