import {StyleSheet} from 'react-native';
import Colors from './colors';

const SharedStyles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.invertPrimary,
    // to hide the WIERD empty space below header when entering route
    // TODO: find a better way
    marginTop: -1,
    paddingTop: 1,
    paddingHorizontal: 8,
  },
  text: {
    color: Colors.primary,
  },
});

export default SharedStyles;
