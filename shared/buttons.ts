const Buttons = {
  paddingVertical: 8,
  paddingHorizontal: 12,
  disabledColor: '#139292',
};

export default Buttons;
