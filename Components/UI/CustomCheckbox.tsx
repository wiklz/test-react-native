import React from 'react';
import {useMemo} from 'react';
import {
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Text,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../../shared/colors';

interface Props {
  value: boolean;
  label: string;
  link?: {
    url: string;
    text: string;
  };
  onChange: () => void;
}

const CustomCheckbox: React.FC<Props> = ({value, label, link, onChange}) => {
  const appliedStyles = useMemo(() => {
    if (value) {
      return [styles.checkbox, styles.checked];
    }
    return [styles.checkbox, styles.unchecked];
  }, [value]);
  return (
    <View style={styles.wrapper}>
      <TouchableWithoutFeedback onPress={onChange}>
        <View style={appliedStyles}>
          <Icon name="check" color={Colors.invertPrimary} size={16} />
        </View>
      </TouchableWithoutFeedback>
      <View style={styles.label}>
        <Text style={styles.labelText}>{label}</Text>
        {/* TODO: add a real redirect */}
        {link && (
          <TouchableOpacity>
            <Text style={styles.linkText}>{link.text}</Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 8,
  },
  checkbox: {
    width: 20,
    height: 20,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  unchecked: {
    borderColor: Colors.primary,
    borderWidth: 1,
  },
  checked: {
    borderColor: Colors.secondary,
    backgroundColor: Colors.secondary,
  },
  label: {
    marginLeft: 16,
  },
  labelText: {
    color: Colors.primary,
  },
  linkText: {
    color: Colors.secondary,
    textDecorationLine: 'underline',
  },
});

export default CustomCheckbox;
