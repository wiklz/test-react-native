import React, {useMemo} from 'react';
import {TextInput, Appearance, StyleSheet} from 'react-native';
import Buttons from '../../shared/buttons';
import Colors from '../../shared/colors';

interface Props {
  type: 'text' | 'email' | 'password' | 'phone';
  value: string;
  onChangeText: (value: string) => void;
}

const CustomInput: React.FC<Props> = ({type, value, onChangeText}) => {
  const keyboardType = useMemo(() => {
    switch (type) {
      case 'email':
        return 'email-address';
      case 'phone':
        return 'phone-pad';

      default:
        return 'default';
    }
  }, [type]);

  const autoCompleteType = useMemo(() => {
    switch (type) {
      case 'email':
        return 'email';
      case 'phone':
        return 'tel';

      default:
        return 'off';
    }
  }, [type]);

  const placeholder = useMemo(() => {
    const val = type;
    return `${val[0].toUpperCase()}${val.slice(1)}`;
  }, [type]);

  const theme = useMemo(
    () => (Appearance.getColorScheme() === 'dark' ? 'dark' : 'light'),
    [],
  );

  return (
    <TextInput
      style={styles.input}
      value={value}
      onChangeText={onChangeText}
      secureTextEntry={type === 'password'}
      keyboardType={keyboardType}
      keyboardAppearance={theme}
      placeholder={placeholder}
      autoCompleteType={autoCompleteType}
    />
  );
};

const styles = StyleSheet.create({
  input: {
    borderColor: `${Colors.primary}7f`,
    borderWidth: 1,
    paddingHorizontal: Buttons.paddingHorizontal,
    paddingVertical: 5,
    marginVertical: 8,
  },
});

export default CustomInput;
