import React, {useState, useCallback} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Buttons from '../../shared/buttons';
import Colors from '../../shared/colors';
import Fonts from '../../shared/fonts';
import CustomInput from '../UI/CustomInput';
import CustomCheckbox from '../UI/CustomCheckbox';

type Props = {
  type: 'login' | 'registration';
  goBack: () => void;
};

const Form: React.FC<Props> = ({type, goBack}) => {
  const [agreementChecked, setAgreementChecked] = useState(false);

  // TODO: сделать метод на submit кнопку вместо заглушки
  // TODO: сделать валидацию инпутов и условие на disable кнопки submit

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const changeEmail = useCallback((value: string) => setEmail(value), []);

  const changePassword = useCallback((value: string) => setPassword(value), []);

  const submit = useCallback(() => {
    console.log('SUBMIT');
  }, []);

  const toggleAgreement = useCallback(
    () => setAgreementChecked(val => !val),
    [],
  );

  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>
        {type === 'login' ? 'Вход' : 'Регистрация'}
      </Text>
      <CustomInput type="email" value={email} onChangeText={changeEmail} />
      <CustomInput
        type="password"
        value={password}
        onChangeText={changePassword}
      />
      {type === 'registration' && (
        <CustomCheckbox
          value={agreementChecked}
          onChange={toggleAgreement}
          label="Я принимаю условия"
          link={{url: '#', text: 'Пользовательского соглашения'}}
        />
      )}
      <TouchableOpacity style={styles.submit} onPress={submit}>
        <Text style={styles.submitText}>
          {type === 'login' ? 'Войти' : 'Зарегистрироваться'}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.back} onPress={goBack}>
        <Text style={styles.backText}>Назад</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    marginHorizontal: 10,
  },
  title: {
    color: Colors.primary,
    textAlign: 'center',
    fontSize: Fonts.sizeTitle,
    fontFamily: Fonts.title,
    marginBottom: 18,
  },
  submit: {
    width: '100%',
    backgroundColor: Colors.secondary,
    paddingHorizontal: Buttons.paddingHorizontal,
    paddingVertical: Buttons.paddingVertical,
    marginTop: 18,
    marginBottom: 26,
  },
  submitText: {
    color: Colors.buttonText,
    textAlign: 'center',
  },
  back: {
    alignSelf: 'center',
    width: 50,
  },
  backText: {
    color: Colors.secondary,
    textAlign: 'center',
  },
});

export default Form;
