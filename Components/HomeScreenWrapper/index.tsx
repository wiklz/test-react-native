import React from 'react';
import {View, StyleSheet} from 'react-native';
import IconsSet from './IconsSet';
import Logo from './Logo';
import Colors from '../../shared/colors';
import SharedStyles from '../../shared/style';

const HomeScreenWrapper: React.FC = ({children}) => {
  return (
    <View style={[SharedStyles.screen, styles.wrapper]}>
      <View style={styles.upperCircle} />
      <IconsSet />
      <Logo />
      <View style={styles.content}>{children}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: 'space-between',
  },
  upperCircle: {
    width: 800,
    height: 800,
    borderRadius: 400,
    position: 'absolute',
    top: -500,
    alignSelf: 'center',
    backgroundColor: Colors.secondary,
  },
  content: {
    alignSelf: 'center',
  },
});

export default HomeScreenWrapper;
