import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';
import Fonts from '../../shared/fonts';

const Logo: React.FC = () => {
  return (
    <View style={styles.wrapper}>
      <Image
        resizeMode="contain"
        style={styles.image}
        source={require('../../assets/images/logo.png')}
      />
      <Text style={styles.appName}>MONEON</Text>
      <Text style={styles.description}>
        Moneon это приложение для учета личных и семейных финансов!
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    alignItems: 'center',
    height: 300,
  },
  appName: {
    fontFamily: Fonts.logo,
    fontSize: 20,
    textAlign: 'center',
    marginTop: -65,
    color: '#262626',
  },
  description: {
    width: 300,
    fontFamily: Fonts.regular,
    fontSize: Fonts.sizePrimary,
    textAlign: 'center',
    marginTop: 26,
    color: '#262626',
  },
  image: {
    width: '25%',
  },
});

export default Logo;
