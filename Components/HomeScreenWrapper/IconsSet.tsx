import React from 'react';
import {View, Image, StyleSheet} from 'react-native';

const HomeScreenWrapper: React.FC = () => {
  return (
    <>
      <View style={styles.chat}>
        <Image
          style={styles.chatIcon}
          resizeMode="contain"
          source={require('../../assets/images/chat.png')}
        />
      </View>
      <View style={styles.star}>
        <Image
          style={styles.starIcon}
          resizeMode="contain"
          source={require('../../assets/images/star.png')}
        />
      </View>
      <View style={styles.plane}>
        <Image
          style={styles.planeIcon}
          resizeMode="contain"
          source={require('../../assets/images/plane.png')}
        />
      </View>
      <View style={styles.cheese}>
        <Image
          style={styles.cheeseIcon}
          resizeMode="contain"
          source={require('../../assets/images/cheese.png')}
        />
      </View>
      <View style={styles.circle}>
        <Image
          style={styles.circleIcon}
          resizeMode="contain"
          source={require('../../assets/images/circle.png')}
        />
      </View>
      <View style={styles.blink1}>
        <Image
          style={styles.blinkIcon}
          resizeMode="contain"
          source={require('../../assets/images/blink.png')}
        />
      </View>
      <View style={styles.blink2}>
        <Image
          style={styles.blinkIcon}
          resizeMode="contain"
          source={require('../../assets/images/blink.png')}
        />
      </View>
      <View style={styles.blink3}>
        <Image
          style={styles.blinkIcon}
          resizeMode="contain"
          source={require('../../assets/images/blink.png')}
        />
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  chat: {
    position: 'absolute',
    top: 243,
    right: 50,
    transform: [
      {
        rotate: '-10deg',
      },
    ],
  },
  chatIcon: {
    width: 20,
  },
  star: {
    position: 'absolute',
    top: 50,
    left: 50,
  },
  starIcon: {
    width: 30,
  },
  plane: {
    position: 'absolute',
    top: 110,
    right: 50,
  },
  planeIcon: {
    width: 35,
  },
  cheese: {
    position: 'absolute',
    top: 250,
    left: 70,
  },
  cheeseIcon: {
    width: 25,
  },
  circle: {
    position: 'absolute',
    top: 265,
    left: '40%',
  },
  circleIcon: {
    width: 25,
  },
  blink1: {
    position: 'absolute',
    top: 45,
    right: '15%',
  },
  blink2: {
    position: 'absolute',
    top: 235,
    right: '40%',
  },
  blink3: {
    position: 'absolute',
    top: 175,
    left: '15%',
  },
  blinkIcon: {
    width: 10,
  },
});

export default HomeScreenWrapper;
