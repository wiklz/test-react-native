import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './screens/HomeScreen';
import AuthScreen from './screens/AuthScreen';
import {RootStackParamList} from './types/navigation';
import Colors from './shared/colors';

const Stack = createStackNavigator<RootStackParamList>();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
          headerStyle: {
            backgroundColor: Colors.invertPrimary,
            shadowColor: Colors.invertPrimary,
          },
          headerTintColor: Colors.secondary,
        }}>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{title: 'Home screen'}}
        />
        <Stack.Screen
          name="Auth"
          component={AuthScreen}
          options={{title: ''}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
