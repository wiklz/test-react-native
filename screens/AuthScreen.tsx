import React, {useEffect} from 'react';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {View} from 'react-native';
import {RootStackParamList} from '../types/navigation';
import SharedStyles from '../shared/style';
import AuthForm from '../Components/AuthForm';
import {useCallback} from 'react';

type Props = {
  navigation: StackNavigationProp<RootStackParamList, 'Auth'>;
  route: RouteProp<RootStackParamList, 'Auth'>;
};

const AuthScreen: React.FC<Props> = ({route, navigation}) => {
  useEffect(() => {
    navigation.setOptions({headerShown: true});
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const goBack = useCallback(() => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    }
  }, [navigation]);

  return (
    <View style={SharedStyles.screen}>
      <AuthForm type={route.params.type} goBack={goBack} />
    </View>
  );
};

export default AuthScreen;
