import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from '../types/navigation';
import HomeScreenWrapper from '../Components/HomeScreenWrapper';
import Buttons from '../shared/buttons';
import Colors from '../shared/colors';
import Fonts from '../shared/fonts';

type Props = {
  navigation: StackNavigationProp<RootStackParamList, 'Home'>;
};

const HomeScreen: React.FC<Props> = ({navigation}) => {
  const redirect = (type: 'registration' | 'login') => {
    if (type === 'registration') {
      navigation.navigate('Auth', {type: 'registration'});
    }
    if (type === 'login') {
      navigation.navigate('Auth', {type: 'login'});
    }
  };

  return (
    <HomeScreenWrapper>
      <View style={styles.wrapper}>
        <Text style={styles.text}>
          Если у тебя есть учетная запись, то авторизуйся.
        </Text>
        <Text style={styles.text}>
          А если ты у нас впервые, то нужно пройти простую регистрацию.
        </Text>
        <TouchableOpacity
          style={styles.registration}
          onPress={() => redirect('registration')}>
          <Text style={styles.registrationText}>Зарегистрироваться</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.login}
          onPress={() => redirect('login')}>
          <Text style={styles.loginText}>Войти</Text>
        </TouchableOpacity>
      </View>
    </HomeScreenWrapper>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    maxWidth: 300,
  },
  text: {
    fontFamily: Fonts.regular,
    fontSize: Fonts.sizePrimary,
    color: Colors.primary,
    textAlign: 'center',
    marginVertical: 6,
  },
  registration: {
    width: '100%',
    backgroundColor: Colors.secondary,
    paddingHorizontal: Buttons.paddingHorizontal,
    paddingVertical: Buttons.paddingVertical,
    marginTop: 18,
    marginBottom: 26,
  },
  registrationText: {
    color: Colors.buttonText,
    textAlign: 'center',
  },
  login: {
    marginBottom: 75,
  },
  loginText: {
    color: Colors.secondary,
  },
});

export default HomeScreen;
